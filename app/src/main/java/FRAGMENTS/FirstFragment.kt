package FRAGMENTS

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.DRAGONFLY.R

class FirstFragment:Fragment(R.layout.first_fragment) {

    private lateinit var noteEditText: EditText
    private lateinit var buttonAdd: Button
    private lateinit var textView: TextView
    private lateinit var sharedPreferences: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
        registerListener()


        sharedPreferences = requireActivity().getSharedPreferences("MY_APP_P", AppCompatActivity.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTES","")
        textView.text = text

    }

    private fun init(view: View){
        textView = view.findViewById(R.id.textView)
        buttonAdd = view.findViewById(R.id.buttonAdd)
        noteEditText = view.findViewById(R.id.noteEditText)
    }


    private fun registerListener(){
        buttonAdd.setOnClickListener {
            val note = textView.text.toString()
            val text = noteEditText.text.toString()
            val result = note+"\n"+text
            textView.text = result

            sharedPreferences.edit()
                .putString("NOTES",result)
                .apply()

        }
    }

}