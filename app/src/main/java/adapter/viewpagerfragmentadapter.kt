package adapter

import FRAGMENTS.FirstFragment
import FRAGMENTS.SecondFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


class viewpagerfragmentadapter (activity: FragmentActivity): FragmentStateAdapter(activity) {
    override fun getItemCount() = 3



    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> FirstFragment()
            1->SecondFragment()

            else -> FirstFragment()
        }

    }
}
