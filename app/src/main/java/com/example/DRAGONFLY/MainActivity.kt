package com.example.DRAGONFLY

import adapter.viewpagerfragmentadapter
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ShareActionProvider
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var tabs:TabLayout
    private lateinit var viewPager:ViewPager2
    private lateinit var viewpagerfragmentadapter: viewpagerfragmentadapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()


        viewPager.adapter=viewpagerfragmentadapter

        TabLayoutMediator(tabs,viewPager){tab,position->
            tab.text="Tab ${position+1}"
        }.attach()

    }
    private fun init(){

        tabs=findViewById(R.id.tabs)
        viewPager=findViewById(R.id.viewPager)
        viewpagerfragmentadapter=viewpagerfragmentadapter(this)

    }
}
